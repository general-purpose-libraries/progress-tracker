package com.gitlab.marvin.progresstracker.impl;

import com.gitlab.marvin.progresstracker.api.BackgroundProgressCollector;
import com.gitlab.marvin.progresstracker.api.ProgressCollector;
import com.gitlab.marvin.progresstracker.api.ProgressReceiver;
import com.gitlab.marvin.progresstracker.api.ProgressSender;
import com.gitlab.marvin.progresstracker.utils.Progress;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;

/**
 * Created by chief on 22.04.17.
 */
public class AbstractBackgroundProgressCollectorIT {
    final BackgroundProgressCollector progressCollector = new AbstractBackgroundProgressCollector();
    private final List<Progress> progressList = new LinkedList<>();
    private final Progress[] progresses = new Progress[]{
            new Progress(BigInteger.valueOf(0), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(20), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(40), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(60), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(80), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(100), BigInteger.valueOf(100))
    };

    private class TestProgressSender implements ProgressSender{
        private final List<BackgroundProgressCollector> progressCollectors = new LinkedList<>();

        @Override
        public void registerProgressCollector(ProgressCollector progressCollector) {
            progressCollectors.add((BackgroundProgressCollector) progressCollector);
        }

        @Override
        public void unregisterProgressCollector(ProgressCollector progressCollector) {
            progressCollectors.remove(progressCollector);
        }

        @Override
        public void sendProgress(Progress progress) {
            for (BackgroundProgressCollector progressCollector : progressCollectors){
                progressCollector.reportProgress(this, progress);
            }
        }
    }

    private class TestProgressReceiver implements ProgressReceiver{
        @Override
        public void notifiy(Progress progress) {
            progressList.add(progress);
        }
    }

    @Test
    void run() throws InterruptedException {
        final BackgroundProgressCollector progressCollector = new AbstractBackgroundProgressCollector();
        final ProgressSender sender1 = new TestProgressSender();
        final ProgressSender sender2 = new TestProgressSender();

        sender1.registerProgressCollector(progressCollector);
        sender2.registerProgressCollector(progressCollector);

        final ProgressReceiver receiver = new TestProgressReceiver();
        progressCollector.registerProgressReceiver(receiver);
        progressCollector.setUpdateIntervalMillis(1);

        ExecutorService executorService = Executors.newWorkStealingPool();
        Future[] futures = new Future[]{
            executorService.submit(progressCollector),
            executorService.submit(() -> {
            for (int a = 0; a < progresses.length; a++) {
                sender1.sendProgress(progresses[a]);
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }),
        executorService.submit(() -> {
            for (int a = 0; a < progresses.length; a++) {
                sender2.sendProgress(progresses[a]);
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        })
        };

        boolean finished = false;
        while (!finished){
            finished = true;
            for (Future future : futures){
                if (!future.isDone() && !future.isCancelled()) {
                    finished = false;
                    break;
                }
            }
        }

        List<Double> progresses = new LinkedList<>();
        for (Progress progress : progressList){
            progresses.add(progress.getProgress());
        }
        List<Double> expected = new LinkedList<>(progresses);
        Collections.sort(expected);

        assertEquals(expected, progresses);
    }

}