package com.gitlab.marvin.progresstracker.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 * This class represents a progress
 */
public class Progress{
    private final BigInteger stepsDone; /*  Amount of steps having been done.
                                            Can be null if not known.*/
    private final BigInteger stepsTotal; /*  amount of steps totally having
                                            to be done (this is the sum of
                                            steps done plus remaining steps).
                                            Can be null if not known.*/
    private final double progress;      //progress in percent

    public Progress(){
        this.stepsDone = null;
        this.stepsTotal = null;
        this.progress = 0;
    }

    public Progress(BigInteger stepsDone, BigInteger stepsTotal) {
        if (stepsDone == null){
            throw new RuntimeException("Parameter  'stepsDone' must not be null");
        }
        if (stepsTotal == null){
            throw new RuntimeException("Parameter 'stepsTotal' must not be null");
        }
        if (stepsDone.compareTo(stepsTotal) > 0){
            throw new RuntimeException("Parameter 'stepsDone' must not be greater than 'stepsTotal'");
        }
        this.stepsDone = stepsDone;
        this.stepsTotal = stepsTotal;
        this.progress = new BigDecimal(stepsDone).divide(
                new BigDecimal(stepsTotal), 15, RoundingMode.HALF_UP).doubleValue() * 100;
        assert this.progress >= 0;
    }

    public BigInteger getStepsDone() {
        return stepsDone;
    }

    public BigInteger getStepsTotal() {
        return stepsTotal;
    }

    public double getProgress() {
        return progress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Progress progress1 = (Progress) o;

        if (Double.compare(progress1.progress, progress) != 0) return false;
        if (stepsDone != null ? !stepsDone.equals(progress1.stepsDone) : progress1.stepsDone != null) return false;
        return stepsTotal != null ? stepsTotal.equals(progress1.stepsTotal) : progress1.stepsTotal == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = stepsDone != null ? stepsDone.hashCode() : 0;
        result = 31 * result + (stepsTotal != null ? stepsTotal.hashCode() : 0);
        temp = Double.doubleToLongBits(progress);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return String.format("Progress: %s of %s (%s%%)", stepsDone, stepsTotal, progress);
    }
}