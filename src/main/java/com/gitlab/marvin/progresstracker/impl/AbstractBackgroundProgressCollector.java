package com.gitlab.marvin.progresstracker.impl;

import com.gitlab.marvin.progresstracker.api.BackgroundProgressCollector;
import com.gitlab.marvin.progresstracker.api.ProgressReceiver;
import com.gitlab.marvin.progresstracker.api.ProgressSender;
import com.gitlab.marvin.progresstracker.utils.Progress;

import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by chief on 14.04.17.
 */
public class AbstractBackgroundProgressCollector implements BackgroundProgressCollector {
    private final ConcurrentMap<ProgressSender, Progress> senderProgress;
    private final Set<ProgressReceiver> progressReceivers;
    private final AtomicBoolean finished = new AtomicBoolean(false);
    private final AtomicBoolean running = new AtomicBoolean(false);
    private final AtomicReference<Long> updateIntervalMillis = new AtomicReference<>(40L);
    private final AtomicBoolean changed = new AtomicBoolean(true);

    public AbstractBackgroundProgressCollector() {
        senderProgress = new ConcurrentHashMap<>();
        progressReceivers = new HashSet<>();
    }

    @Override
    public void reportProgress(ProgressSender sender, Progress progress) {
        if (sender == null){
            throw new RuntimeException("Parameter 'sender' must not be null");
        }
        if (progress == null){
            throw new RuntimeException("Parameter 'progress' must not be null");
        }
        Progress oldValue = senderProgress.put(sender, progress);
        if (oldValue == null || !oldValue.equals(progress)){
            changed.compareAndSet(false, true);
        }
    }

    @Override
    public void registerProgressReceiver(ProgressReceiver receiver) {
        if (receiver == null){
            throw new RuntimeException("Parameter 'receiver' must not be null");
        }
        progressReceivers.add(receiver);
    }

    @Override
    public void unregisterProgressReceiver(ProgressReceiver receiver) {
        if (receiver == null){
            throw new RuntimeException("Parameter 'receiver' must not be null");
        }
        progressReceivers.remove(receiver);
    }

    @Override
    public void sendProgress() {
        Progress progress = calculateProgress();
        for (ProgressReceiver receiver : progressReceivers){
            receiver.notifiy(progress);
        }

        if (progress.getStepsDone() != null && progress.getStepsTotal() != null){
            if (progress.getStepsDone().compareTo(progress.getStepsTotal()) >= 0
                    && progress.getStepsTotal().compareTo(BigInteger.ZERO) > 0){
                finished.set(true);
            }
        }
    }

    private Progress calculateProgress() {
        BigInteger stepsTotal = BigInteger.ZERO;
        BigInteger stepsDone = BigInteger.ZERO;

        List<Progress> values = new LinkedList<>(senderProgress.values());
        for (Progress progress : values){
            stepsTotal = stepsTotal.add(progress.getStepsTotal());
            stepsDone = stepsDone.add(progress.getStepsDone());
        }
        if (stepsTotal.compareTo(BigInteger.ZERO) <= 0){
            return new Progress();
        }
        return new Progress(stepsDone, stepsTotal);
    }

    @Override
    public void run() {
        finished.set(false);
        running.set(true);
        changed.set(true);

        while (!finished.get()){
            if (changed.compareAndSet(true, false)){
                sendProgress();
            }
            try {
                Thread.sleep(updateIntervalMillis.get());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        sendProgress();

        running.set(false);
    }

    @Override
    public long getUpdateIntervalMillis() {
        return updateIntervalMillis.get();
    }

    @Override
    public void setUpdateIntervalMillis(long updateIntervalMillis) {
        this.updateIntervalMillis.set(updateIntervalMillis);
    }
}
