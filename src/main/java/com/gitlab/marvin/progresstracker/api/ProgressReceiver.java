package com.gitlab.marvin.progresstracker.api;

import com.gitlab.marvin.progresstracker.utils.Progress;

import java.math.BigDecimal;

/**
 * Interface describing a listener listing to progress changes
 */
public interface ProgressReceiver {
    /**
     * Notifies the progress listener about a progress change
     * @param progress Current progress in percent
     */
    void notifiy(Progress progress);
}
